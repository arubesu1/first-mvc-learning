using System.ComponentModel.DataAnnotations;

namespace form_example.Models
{
    public class Product
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public float Price { get; set; }

    }
}