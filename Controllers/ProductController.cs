using form_example.Models;
using Microsoft.AspNetCore.Mvc;

namespace form_example.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public IActionResult Save()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save(Product product)
        {
            if(!ModelState.IsValid)
                ViewBag.Validator = "Invalid Product";
            
                return View();
        }
    }
}